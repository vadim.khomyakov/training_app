import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigatedData } from "tns-core-modules/ui/frame/frame";
import { Router } from "@angular/router";
import { TrainingService } from "~/app/services/training-service";
import { Training } from "~/app/modals/training";

@Component({
    selector: "Details",
    styleUrls: ["./details.component.css"],
    templateUrl: "./details.component.html"
})
export class DetailsComponent implements OnInit {

    private trainingId: number;
    public training: Training;

    constructor(
        private routerExtensions: RouterExtensions,
        private activatedRoute: Router,
        private trainingStervice: TrainingService,

    ) {
        const params = activatedRoute.parseUrl(activatedRoute.url).queryParams;
        this.trainingId = params['id'];
    }

    ngOnInit(): void {
        this.training = this.trainingStervice.getTraining(this.trainingId);
    }

    public goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}
