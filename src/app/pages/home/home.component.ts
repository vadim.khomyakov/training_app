import { Component, OnInit } from "@angular/core";
import { TrainingService } from "~/app/services/training-service";
import { Training } from "~/app/modals/training";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Home",
    styleUrls: ["./home.component.css"],
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    private items: Array<Training>;

    constructor(
        private router: RouterExtensions,
        private trainingStervice: TrainingService,
    ) { }

    ngOnInit(): void {
        this.items = this.trainingStervice.getTrainigs();
    }

    public message: string = "Hello, Angular!";
    public onTap() {
        this.message = "OHAI";
    }

    onItemTap(args: ItemEventData) {
        console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
        const item = this.items[args.index];
        this.router.navigate(['/details'], {
            queryParams: {
                id: item.id
            },
            queryParamsHandling: "merge",
        });
    }
}
